package Controller;

import java.util.Random;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

public class BackEnd 
{
    private static final Random random = new Random();
    
    private static int GenerateRandom(String input) 
    {
        String[] a = input.split("-");
        int min = Integer.parseInt(a[0]);
        int max = Integer.parseInt(a[1]);
        return random.nextInt((max - min) + 1) + min;
    }
    
    public static String getJson(String monsterId) 
    {
        JsonObjectBuilder jobj = Json.createObjectBuilder();
        jobj.add("Szorny id", GenerateRandom(monsterId));
        JsonObject complete = jobj.build();
        return complete.toString();
    }
}