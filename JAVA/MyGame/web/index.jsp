<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="Controller.BackEnd"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JAVA</title>
        <body>
        <%
            if (request.getParameter("monsterId") == null) {
                    out.println("Hibás paraméterekk!");
            } else {
                ServletContext context = getServletContext();
                RequestDispatcher rd = context.getRequestDispatcher("/InputHandler");
                rd.forward(request, response);
            }
            %>
        </body>
    </head>
</html>