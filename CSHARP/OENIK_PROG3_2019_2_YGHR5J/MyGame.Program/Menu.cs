﻿// <copyright file="Menu.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyGame.Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MyGame.Logic;

    /// <summary>
    /// Menu.
    /// </summary>
    public class Menu
    {
        private GameLogic logic = new GameLogic();

        private string[] startMenu = new string[]
            {
                "  CRUD műveletek  ",
                "NON-CRUD műveletek",
                "       JAVA       ",
                "      Kilépés     ",
            };

        private string[] crudMenu = new string[]
            {
                "      Játékosok  listázása      ",
                "       Játékos hozzáadása       ",
                "       Játékos módosítása       ",
                "        Játékos törlése         ",
                "       Szörnyek listázása       ",
                "       Szörny  hozzáadása       ",
                "       Szörny  módosítása       ",
                "         Szörny törlése         ",
                "        Elemek listázása        ",
                "        Elem  hozzáadása        ",
                "        Elem  módosítása        ",
                "          Elem törlése          ",
                "Játékosok  szörnyeinek listázása",
                "Szörny hozzáadása egy játékoshoz",
                " Játékos szörnyének módosítása  ",
                "  Játékos  szörnyének törlése   ",
                "             Vissza             ",
            };

        private string[] nonCrudMenu = new string[]
            {
                "   Egy  játékos osszes szörnyének listázása   ",
                "Típusonként a játékosoknál lévő szörnyek száma",
                "            Szörnyek átlagos élete            ",
                "                    Vissza                    ",
            };

        /// <summary>
        /// Menu.
        /// </summary>
        public void Start()
        {
            this.StartMenu();
        }

        private void StartMenu()
        {
            Console.Clear();
            bool end = false;
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("  CRUD műveletek  ");
            Console.ResetColor();
            Console.WriteLine("NON-CRUD műveletek");
            Console.WriteLine("       JAVA       ");
            Console.WriteLine("      Kilépés     ");
            int y = 0;
            Console.SetCursorPosition(0, y);
            int line = 0;
            Console.CursorVisible = false;
            do
            {
                ConsoleKeyInfo key = Console.ReadKey(true);
                if (key.Key == ConsoleKey.DownArrow)
                {
                    if (y != 3)
                    {
                        y++;
                        Console.ResetColor();
                        Console.WriteLine(this.startMenu[line]);
                        Console.ForegroundColor = ConsoleColor.Blue;
                        Console.WriteLine(this.startMenu[++line]);
                        Console.SetCursorPosition(0, y);
                    }
                }
                else if (key.Key == ConsoleKey.UpArrow)
                {
                    if (y != 0)
                    {
                        y--;
                        Console.ResetColor();
                        Console.WriteLine(this.startMenu[line]);
                        Console.ForegroundColor = ConsoleColor.Blue;
                        Console.SetCursorPosition(0, y);
                        Console.WriteLine(this.startMenu[--line]);
                        Console.SetCursorPosition(0, y);
                    }
                }
                else if (key.Key == ConsoleKey.Enter)
                {
                    if (y == 0)
                    {
                        this.CrudMenu();
                        break;
                    }
                    else if (y == 1)
                    {
                        this.NonCrudMenu();
                        break;
                    }
                    else if (y == 2)
                    {
                        Console.Clear();
                        Console.ResetColor();
                        Console.WriteLine("Adja meg a játékos ID-jét, aki kap egy meglepetés szörnyet.");
                        string id = Console.ReadLine();
                        int playerId;
                        while (id == string.Empty || !int.TryParse(id, out playerId))
                        {
                            Console.WriteLine("Nem megfelelő.");
                            id = Console.ReadLine();
                        }

                        try
                        {
                            this.logic.RandomMonsterToAPlayer(playerId);
                            Console.WriteLine("\nA jatákos megkapta a szörnyet.");
                        }
                        catch (NullReferenceException e)
                        {
                            Console.WriteLine(e.Message);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("HIBA" + e.Message);
                        }

                        Console.WriteLine("Visszalépshez nymoja meg az ENTER-t...");
                        Console.ReadLine();
                        this.StartMenu();
                        break;
                    }
                    else
                    {
                        end = true;
                        break;
                    }
                }
            }
            while (!end);
        }

        private void CrudMenu()
        {
            Console.Clear();
            bool end = false;
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("      Játékosok  listázása      ");
            Console.ResetColor();
            Console.WriteLine("       Játékos hozzáadása       ");
            Console.WriteLine("       Játékos módosítása       ");
            Console.WriteLine("        Játékos törlése         ");
            Console.WriteLine("       Szörnyek listázása       ");
            Console.WriteLine("       Szörny  hozzáadása       ");
            Console.WriteLine("       Szörny  módosítása       ");
            Console.WriteLine("         Szörny törlése         ");
            Console.WriteLine("        Elemek listázása        ");
            Console.WriteLine("        Elem  hozzáadása        ");
            Console.WriteLine("        Elem  módosítása        ");
            Console.WriteLine("          Elem törlése          ");
            Console.WriteLine("Játékosok  szörnyeinek listázása");
            Console.WriteLine("Szörny hozzáadása egy játékoshoz");
            Console.WriteLine(" Játékos szörnyének módosítása  ");
            Console.WriteLine("  Játékos  szörnyének törlése   ");
            Console.WriteLine("             Vissza             ");
            int y = 0;
            Console.SetCursorPosition(0, y);
            int line = 0;
            Console.CursorVisible = false;
            do
            {
                ConsoleKeyInfo key = Console.ReadKey(true);
                if (key.Key == ConsoleKey.DownArrow)
                {
                    if (y != 16)
                    {
                        y++;
                        Console.ResetColor();
                        Console.WriteLine(this.crudMenu[line]);
                        Console.ForegroundColor = ConsoleColor.Blue;
                        Console.WriteLine(this.crudMenu[++line]);
                        Console.SetCursorPosition(0, y);
                    }
                }
                else if (key.Key == ConsoleKey.UpArrow)
                {
                    if (y != 0)
                    {
                        y--;
                        Console.ResetColor();
                        Console.WriteLine(this.crudMenu[line]);
                        Console.ForegroundColor = ConsoleColor.Blue;
                        Console.SetCursorPosition(0, y);
                        Console.WriteLine(this.crudMenu[--line]);
                        Console.SetCursorPosition(0, y);
                    }
                }
                else if (key.Key == ConsoleKey.Enter)
                {
                    Console.ResetColor();
                    if (y == 0)
                    {
                        Console.Clear();
                        try
                        {
                            Console.WriteLine(this.logic.GetAllPlayer());
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("HIBA" + e.Message);
                        }

                        Console.WriteLine("Visszalépshez nymoja meg az ENTER-t...");
                        Console.ReadLine();
                        this.CrudMenu();
                        break;
                    } // Játékosok  listázása
                    else if (y == 1)
                    {
                        Console.Clear();
                        string name;
                        string sd;
                        DateTime startDate;
                        string lvl;
                        int level;
                        string guild;
                        string rank;
                        Console.WriteLine("Adja meg a játékos nevét!");
                        name = Console.ReadLine();
                        while (name == string.Empty || name.Length > 20)
                        {
                            Console.WriteLine("A név hossza nem megfelelő. Adjon meg másikat");
                            name = Console.ReadLine();
                        }

                        Console.WriteLine("Adja meg a játékos kezdésének dátumát! (yyyy.MM.dd)");
                        sd = Console.ReadLine();
                        while (sd == string.Empty || !DateTime.TryParseExact(sd, "yyyy.MM.dd", null, System.Globalization.DateTimeStyles.None, out startDate))
                        {
                            Console.WriteLine("A formátum nem megfelelő. Adja meg helyesen! (yyyy.MM.dd)");
                            sd = Console.ReadLine();
                        }

                        Console.WriteLine("Adja meg a játékos szintjét! (szám: 1-100)");
                        lvl = Console.ReadLine();
                        while (lvl == string.Empty || !int.TryParse(lvl, out level) || level < 1 || level > 100)
                        {
                            Console.WriteLine("Nem megfelelő. Adja meg helyesen (szám: 1-100)");
                            lvl = Console.ReadLine();
                        }

                        Console.WriteLine("Adja meg a játékos klánját!");
                        guild = Console.ReadLine();

                        Console.WriteLine("Adja meg a játékos rangját!\n(bronz, ezust, arany, gyemant)");
                        rank = Console.ReadLine();
                        while (!(rank == string.Empty || rank == "bronz" || rank == "ezust" || rank == "arany" || rank == "gyemant"))
                        {
                            Console.WriteLine("Ilyen rang nem létezik, válasszon az alábbiak közül: bronz, ezust, arany, gyemant");
                            rank = Console.ReadLine();
                        }

                        Console.Clear();
                        string date = startDate.ToString("yyyy.MM.dd");
                        Console.WriteLine($"Játékos: {name} - {date} - {level} - {guild} - {rank}");
                        Console.WriteLine("A hozzáadáshoz írjon be egy i-t.");
                        string submit = Console.ReadLine();
                        if (submit == "i")
                        {
                            try
                            {
                                this.logic.AddPlayer(name, date, level, guild, rank);
                                Console.WriteLine("Hozzáadva.");
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine("HIBA: " + e.Message);
                            }
                        }
                        else
                        {
                            Console.WriteLine("A játékos nem lett hozzáadva.");
                        }

                        Console.WriteLine("Továbblépéshez nymoja meg az ENTER-t...");
                        Console.ReadLine();
                        this.CrudMenu();
                        break;
                    } // Játékos hozzáadása
                    else if (y == 2)
                    {
                        Console.Clear();
                        string oldId;
                        int oldPlayerId;
                        do
                        {
                            Console.WriteLine("Adja meg a módosítani kívánt játékos ID-jét. (szám)");
                            oldId = Console.ReadLine();
                        }
                        while (oldId == string.Empty || !int.TryParse(oldId, out oldPlayerId));
                        var player = this.logic.GetPlayerById(oldPlayerId);
                        if (player != null)
                        {
                            Console.Clear();
                            string name;
                            string sd;
                            DateTime startDate;
                            string lvl;
                            int level;
                            string guild;
                            string rank;
                            Console.WriteLine("Adja meg a játékos nevét!");
                            name = Console.ReadLine();
                            while (name == string.Empty || name.Length > 20)
                            {
                                Console.WriteLine("A név hossza nem megfelelő. Adjon meg másikat");
                                name = Console.ReadLine();
                            }

                            Console.WriteLine("Adja meg a játékos kezdésének dátumát! (yyyy.MM.dd)");
                            sd = Console.ReadLine();
                            while (sd == string.Empty || !DateTime.TryParseExact(sd, "yyyy.MM.dd", null, System.Globalization.DateTimeStyles.None, out startDate))
                            {
                                Console.WriteLine("A formátum nem megfelelő. Adja meg helyesen! (yyyy.MM.dd)");
                                sd = Console.ReadLine();
                            }

                            Console.WriteLine("Adja meg a játékos szintjét! (szám: 1-100)");
                            lvl = Console.ReadLine();
                            while (lvl == string.Empty || !int.TryParse(lvl, out level) || level < 1 || level > 100)
                            {
                                Console.WriteLine("Nem megfelelő. Adja meg helyesen (szám: 1-100)");
                                lvl = Console.ReadLine();
                            }

                            Console.WriteLine("Adja meg a játékos klánját!");
                            guild = Console.ReadLine();

                            Console.WriteLine("Adja meg a játékos rangját!\n(bronz, ezust, arany, gyemant)");
                            rank = Console.ReadLine();
                            while (!(rank == string.Empty || rank == "bronz" || rank == "ezust" || rank == "arany" || rank == "gyemant"))
                            {
                                Console.WriteLine("Ilyen rang nem létezik, válasszon az alábbiak közül: bronz, ezust, arany, gyemant");
                                rank = Console.ReadLine();
                            }

                            string date = startDate.ToString("yyyy.MM.dd");
                            Console.Clear();
                            Console.WriteLine("Módosítani kívánt játékos: " + player);
                            Console.WriteLine($"Módosított játékos: {name} - {startDate} - {level} - {guild} - {rank}");
                            Console.WriteLine($"A módosításhoz írjon be egy i-t.");
                            string submit = Console.ReadLine();
                            if (submit == "i")
                            {
                                try
                                {
                                    this.logic.UpdatePlayer(oldPlayerId, name, date, level, guild, rank);
                                    Console.WriteLine("Módosítva.");
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine("HIBA: " + e.Message);
                                }
                            }
                            else
                            {
                                Console.WriteLine("A játékos nem lett módosítva.");
                            }
                        }
                        else
                        {
                            Console.Clear();
                            Console.WriteLine("Nincs ilyen játékos.");
                        }

                        Console.WriteLine("Továbblépéshez nymoja meg az ENTER-t...");
                        Console.ReadLine();
                        this.CrudMenu();
                        break;
                    } // Játékos módosítása
                    else if (y == 3)
                    {
                        string id;
                        int playerId;
                        Console.Clear();
                        do
                        {
                            Console.WriteLine("Adja meg a törölni kívánt játékos ID-jét. (szám)");
                            id = Console.ReadLine();
                        }
                        while (id == string.Empty || !int.TryParse(id, out playerId));
                        Console.Clear();
                        var player = this.logic.GetPlayerById(playerId);
                        if (player != null)
                        {
                            Console.WriteLine("Törölni kívánt játékos: " + player);
                            Console.WriteLine("A törléshez irjon be egy i-t.");
                            string submit = Console.ReadLine();
                            if (submit == "i")
                            {
                                try
                                {
                                    this.logic.DeletePlayer(playerId);
                                    Console.WriteLine("Törölve.");
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine("HIBA: " + e.Message);
                                }  
                            }
                            else
                            {
                                Console.WriteLine("A játékos nem lett törölve.");
                            }
                        }
                        else
                        {
                            Console.WriteLine("Nincs ilyen játékos.");
                        }

                        Console.WriteLine("Továbblépéshez nymoja meg az ENTER-t...");
                        Console.ReadLine();
                        this.CrudMenu();
                        break;
                    } // Játékos törlése
                    else if (y == 4)
                    {
                        Console.Clear();
                        try
                        {
                            Console.WriteLine(this.logic.GetAllMonster());
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("HIBA: " + e.Message);
                        }

                        Console.WriteLine("Visszalépshez nymoja meg az ENTER-t...");
                        Console.ReadLine();
                        this.CrudMenu();
                        break;
                    } // Szörnyek listázása
                    else if (y == 5)
                    {
                        Console.Clear();
                        string name;
                        string eId;
                        int elementId;
                        string type;
                        string hp;
                        int hitpoints;
                        string att;
                        int attack;
                        string def;
                        int defense;
                        Console.WriteLine("Adja meg a szörny nevét!");
                        name = Console.ReadLine();
                        while (name == string.Empty || name.Length > 20)
                        {
                            Console.WriteLine("A név hossza nem megfelelő. Adjon meg másikat");
                            name = Console.ReadLine();
                        }

                        Console.WriteLine("Adja meg a szörny elemének ID-jét! (szám)");
                        eId = Console.ReadLine();
                        while (eId == string.Empty || !int.TryParse(eId, out elementId))
                        {
                            Console.WriteLine("Nem megfelelő.");
                            eId = Console.ReadLine();
                        }

                        Console.WriteLine("Adja meg a szörny típusát!\n(tamado, elet, tamogato, vedelem)");
                        type = Console.ReadLine();
                        while (!(type == "tamado" || type == "elet" || type == "tamogato" || type == "vedelem"))
                        {
                            Console.WriteLine("Ilyen típus nem létezik, válasszon az alábbiak közül: tamado, elet, tamogato, vedelem");
                            type = Console.ReadLine();
                        }

                        Console.WriteLine("Adja meg a szörny életét! (szám: 1-16000)");
                        hp = Console.ReadLine();
                        while (hp == string.Empty || !int.TryParse(hp, out hitpoints) || hitpoints < 1 || hitpoints > 16000)
                        {
                            Console.WriteLine("Nem megfelelő. Adja meg helyesen (szám: 1-16000)");
                            hp = Console.ReadLine();
                        }

                        Console.WriteLine("Adja meg a szörny támadását! (szám: 1-1300)");
                        att = Console.ReadLine();
                        while (att == string.Empty || !int.TryParse(att, out attack) || attack < 1 || attack > 1300)
                        {
                            Console.WriteLine("Nem megfelelő. Adja meg helyesen (szám: 1-1300)");
                            att = Console.ReadLine();
                        }

                        Console.WriteLine("Adja meg a szörny védelmét! (szám: 1-1300)");
                        def = Console.ReadLine();
                        while (def == string.Empty || !int.TryParse(def, out defense) || defense < 1 || defense > 1300)
                        {
                            Console.WriteLine("Nem megfelelő. Adja meg helyesen (szám: 1-1300)");
                            def = Console.ReadLine();
                        }

                        Console.Clear();
                        Console.WriteLine($"Szörny: {name} - {elementId} - {type} - {hitpoints} - {attack} - {defense}");
                        Console.WriteLine("A hozzáadáshoz írjon be egy i-t.");
                        string submit = Console.ReadLine();
                        if (submit == "i")
                        {
                            try
                            {
                                this.logic.AddMonster(name, elementId, type, hitpoints, attack, defense);
                                Console.WriteLine("Hozzáadva.");
                                Console.WriteLine("Továbblépéshez nymoja meg az ENTER-t...");
                                Console.ReadLine();
                                this.CrudMenu();
                                break;
                            }
                            catch (ArgumentException e)
                            {
                                Console.WriteLine(e.Message);
                            }
                        }
                        else
                        {
                            Console.WriteLine("A szörny nem lett hozzáadva.");
                        }

                        Console.WriteLine("Továbblépéshez nymoja meg az ENTER-t...");
                        Console.ReadLine();
                        this.CrudMenu();
                        break;
                    } // Szörny  hozzáadása
                    else if (y == 6)
                    {
                        Console.Clear();
                        string oldId;
                        int oldMonsterId;
                        do
                        {
                            Console.WriteLine("Adja meg a módosítani kívánt szörny ID-jét. (szám)");
                            oldId = Console.ReadLine();
                        }
                        while (oldId == string.Empty || !int.TryParse(oldId, out oldMonsterId));
                        Console.Clear();
                        string name;
                        string eId;
                        int elementId;
                        string type;
                        string hp;
                        int hitpoints;
                        string att;
                        int attack;
                        string def;
                        int defense;
                        Console.WriteLine("Adja meg a szörny nevét!");
                        name = Console.ReadLine();
                        while (name == string.Empty || name.Length > 20)
                        {
                            Console.WriteLine("A név hossza nem megfelelő. Adjon meg másikat");
                            name = Console.ReadLine();
                        }

                        Console.WriteLine("Adja meg a szörny elemének ID-jét! (szám)");
                        eId = Console.ReadLine();
                        while (eId == string.Empty || !int.TryParse(eId, out elementId))
                        {
                            Console.WriteLine("Nem megfelelő.");
                            eId = Console.ReadLine();
                        }

                        Console.WriteLine("Adja meg a szörny típusát!\n(tamado, elet, tamogato, vedelem)");
                        type = Console.ReadLine();
                        while (!(type == "tamado" || type == "elet" || type == "tamogato" || type == "vedelem"))
                        {
                            Console.WriteLine("Ilyen típus nem létezik, válasszon az alábbiak közül: tamado, elet, tamogato, vedelem");
                            type = Console.ReadLine();
                        }

                        Console.WriteLine("Adja meg a szörny életét! (szám: 1-16000)");
                        hp = Console.ReadLine();
                        while (hp == string.Empty || !int.TryParse(hp, out hitpoints) || hitpoints < 1 || hitpoints > 16000)
                        {
                            Console.WriteLine("Nem megfelelő. Adja meg helyesen (szám: 1-16000)");
                            hp = Console.ReadLine();
                        }

                        Console.WriteLine("Adja meg a szörny támadását! (szám: 1-1300)");
                        att = Console.ReadLine();
                        while (att == string.Empty || !int.TryParse(att, out attack) || attack < 1 || attack > 1300)
                        {
                            Console.WriteLine("Nem megfelelő. Adja meg helyesen (szám: 1-1300)");
                            att = Console.ReadLine();
                        }

                        Console.WriteLine("Adja meg a szörny védelmét! (szám: 1-1300)");
                        def = Console.ReadLine();
                        while (def == string.Empty || !int.TryParse(def, out defense) || defense < 1 || defense > 1300)
                        {
                            Console.WriteLine("Nem megfelelő. Adja meg helyesen (szám: 1-1300)");
                            def = Console.ReadLine();
                        }

                        Console.Clear();
                        var monster = this.logic.GetMonsterById(oldMonsterId);
                        if (monster != null)
                        {
                            Console.WriteLine("Módosítani kívánt szörny: " + monster);
                            Console.WriteLine($"Módosított szörny: {name} - {elementId} - {type} - {hitpoints} - {attack} - {defense}");
                            Console.WriteLine("A módosításhoz írjon be egy i-t.");
                            string submit = Console.ReadLine();
                            if (submit == "i")
                            {
                                try
                                {
                                    this.logic.UpdateMonster(oldMonsterId, name, elementId, type, hitpoints, attack, defense);
                                    Console.WriteLine("Módosítva.");
                                }
                                catch (ArgumentException e)
                                {
                                    Console.WriteLine(e.Message);
                                }
                                catch (NullReferenceException e)
                                {
                                    Console.WriteLine(e.Message);
                                }
                            }
                            else
                            {
                                Console.WriteLine("A szörny nem lett módosítva.");
                            }
                        }
                        else
                        {
                            Console.WriteLine("Nincs ilyen szörny.");
                        }

                        Console.WriteLine("Továbblépéshez nymoja meg az ENTER-t...");
                        Console.ReadLine();
                        this.CrudMenu();
                        break;
                    } // Szörny  módosítása
                    else if (y == 7)
                    {
                        string id;
                        int monsterId;
                        Console.Clear();
                        do
                        {
                            Console.WriteLine("Adja meg a törölni kívánt szörny ID-jét. (szám)");
                            id = Console.ReadLine();
                        }
                        while (id == string.Empty || !int.TryParse(id, out monsterId));
                        Console.Clear();
                        var monster = this.logic.GetMonsterById(monsterId);
                        if (monster != null)
                        {
                            Console.WriteLine("Törölni kívánt szörny: " + monster);
                            Console.WriteLine("A törléshez irjon be egy i-t.");
                            string submit = Console.ReadLine();
                            if (submit == "i")
                            {
                                try
                                {
                                    this.logic.DeleteMonster(monsterId);
                                    Console.WriteLine("Törölve.");
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine("HIBA: " + e.Message);
                                }
                            }
                            else
                            {
                                Console.WriteLine("A szörny nem lett törölve.");
                            }
                        }
                        else
                        {
                            Console.WriteLine("Nincs ilyen szörny.");
                        }

                        Console.WriteLine("Továbblépéshez nymoja meg az ENTER-t...");
                        Console.ReadLine();
                        this.CrudMenu();
                        break;
                    } // Szörny törlése
                    else if (y == 8)
                    {
                        Console.Clear();
                        try
                        {
                            Console.WriteLine(this.logic.GetAllElement());
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("HIBA: " + e.Message);
                        }

                        Console.WriteLine("Visszalépshez nymoja meg az ENTER-t...");
                        Console.ReadLine();
                        this.CrudMenu();
                        break;
                    } // Elemek listázása
                    else if (y == 9)
                    {
                        Console.Clear();
                        string name;
                        string str;
                        int strength;
                        string weak;
                        int weakness;
                        string rarity;
                        string color;
                        Console.WriteLine("Adja meg az elem nevét!");
                        name = Console.ReadLine();
                        while (name == string.Empty || name.Length > 20)
                        {
                            Console.WriteLine("A név hossza nem megfelelő. Adjon meg másikat");
                            name = Console.ReadLine();
                        }

                        Console.WriteLine("Adja meg az elem erősségét! (elem id: szám)");
                        str = Console.ReadLine();
                        while (str == string.Empty || !int.TryParse(str, out strength))
                        {
                            Console.WriteLine("Nem megfelelő.");
                            str = Console.ReadLine();
                        }

                        Console.WriteLine("Adja meg az elem gyengeségét! (elem id: szám)");
                        weak = Console.ReadLine();
                        while (weak == string.Empty || !int.TryParse(weak, out weakness))
                        {
                            Console.WriteLine("Nem megfelelő.");
                            weak = Console.ReadLine();
                        }

                        Console.WriteLine("Adja meg az elem ritkaságát!\n(gyakori, ritka, nagyon ritka)");
                        rarity = Console.ReadLine();
                        while (!(rarity == "gyakori" || rarity == "ritka" || rarity == "nagyon ritka"))
                        {
                            Console.WriteLine("Ilyen nem létezik, válasszon az alábbiak közül: gyakori, ritka, nagyon ritka");
                            rarity = Console.ReadLine();
                        }

                        Console.WriteLine("Adja meg az elem színét!");
                        color = Console.ReadLine();
                        while (color == string.Empty || color.Length > 20)
                        {
                            Console.WriteLine("A hossza nem megfelelő. Adjon meg másikat");
                            color = Console.ReadLine();
                        }

                        Console.Clear();
                        Console.WriteLine($"Elem: {name} - {strength} - {weakness} - {rarity} - {color}");
                        Console.WriteLine("A hozzáadáshoz írjon be egy i-t.");
                        string submit = Console.ReadLine();
                        if (submit == "i")
                        {
                            try
                            {
                                this.logic.AddElement(name, strength, weakness, rarity, color);
                                Console.WriteLine("Hozzáadva.");
                            }
                            catch (ArgumentException e)
                            {
                                Console.WriteLine(e.Message);
                            }
                        }
                        else
                        {
                            Console.WriteLine("Az elem nem lett hozzáadva.");
                        }

                        Console.WriteLine("Továbblépéshez nymoja meg az ENTER-t...");
                        Console.ReadLine();
                        this.CrudMenu();
                        break;
                    } // Elem  hozzáadása
                    else if (y == 10)
                    {
                        Console.Clear();
                        string oldId;
                        int oldElementId;
                        do
                        {
                            Console.WriteLine("Adja meg a módosítani kívánt elem ID-jét. (szám)");
                            oldId = Console.ReadLine();
                        }
                        while (oldId == string.Empty || !int.TryParse(oldId, out oldElementId));
                        var element = this.logic.GetElementById(oldElementId);
                        if (element != null)
                        {
                            Console.Clear();
                            string name;
                            string str;
                            int strength;
                            string weak;
                            int weakness;
                            string rarity;
                            string color;
                            Console.WriteLine("Adja meg az elem nevét!");
                            name = Console.ReadLine();
                            while (name == string.Empty || name.Length > 20)
                            {
                                Console.WriteLine("A név hossza nem megfelelő. Adjon meg másikat");
                                name = Console.ReadLine();
                            }

                            Console.WriteLine("Adja meg az elem erősségét! (elem id: szám)");
                            str = Console.ReadLine();
                            while (str == string.Empty || !int.TryParse(str, out strength))
                            {
                                Console.WriteLine("Nem megfelelő.");
                                str = Console.ReadLine();
                            }

                            Console.WriteLine("Adja meg az elem gyengeségét! (elem id: szám)");
                            weak = Console.ReadLine();
                            while (weak == string.Empty || !int.TryParse(weak, out weakness))
                            {
                                Console.WriteLine("Nem megfelelő.");
                                weak = Console.ReadLine();
                            }

                            Console.WriteLine("Adja meg az elem ritkaságát!\n(gyakori, ritka, nagyon ritka)");
                            rarity = Console.ReadLine();
                            while (!(rarity == "gyakori" || rarity == "ritka" || rarity == "nagyon ritka"))
                            {
                                Console.WriteLine("Ilyen nem létezik, válasszon az alábbiak közül: gyakori, ritka, nagyon ritka");
                                rarity = Console.ReadLine();
                            }

                            Console.WriteLine("Adja meg az elem színét!");
                            color = Console.ReadLine();
                            while (color == string.Empty || color.Length > 20)
                            {
                                Console.WriteLine("A hossza nem megfelelő. Adjon meg másikat");
                                color = Console.ReadLine();
                            }

                            Console.WriteLine("Módosítani kívánt szörny: " + element);
                            Console.WriteLine($"Módosított elem: {name} - {strength} - {weakness} - {rarity} - {color}");
                            Console.Clear();
                            Console.WriteLine("A hozzáadáshoz írjon be egy i-t.");
                            string submit = Console.ReadLine();
                            if (submit == "i")
                            {
                                try
                                {
                                    this.logic.UpdateElement(oldElementId, name, strength, weakness, rarity, color);
                                    Console.WriteLine("Módosítva.");
                                }
                                catch (ArgumentException e)
                                {
                                    Console.WriteLine(e.Message);
                                }
                                catch (NullReferenceException e)
                                {
                                    Console.WriteLine(e.Message);
                                }
                            }
                            else
                            {
                                Console.WriteLine("Az elem nem lett módosítva.");
                            }
                        }
                        else
                        {
                            Console.WriteLine("Nincs ilyen elem.");
                        }

                        Console.WriteLine("Továbblépéshez nymoja meg az ENTER-t...");
                        Console.ReadLine();
                        this.CrudMenu();
                        break;
                    } // Elem  módosítása
                    else if (y == 11)
                    {
                        string id;
                        int elementId;
                        Console.Clear();
                        do
                        {
                            Console.WriteLine("Adja meg a törölni kívánt elem ID-jét. (szám)");
                            id = Console.ReadLine();
                        }
                        while (id == string.Empty || !int.TryParse(id, out elementId));
                        var element = this.logic.GetElementById(elementId);
                        if (element != null)
                        {
                            Console.Clear();
                            Console.WriteLine("Törölni kívánt elem: " + element);
                            Console.WriteLine("A törléshez irjon be egy i-t.");
                            string submit = Console.ReadLine();
                            if (submit == "i")
                            {
                                try
                                {
                                    this.logic.DeleteElement(elementId);
                                    Console.WriteLine("Törölve.");
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine("HIBA: " + e.Message);
                                }
                            }
                            else
                            {
                                Console.WriteLine("Az elem nem lett törölve.");
                            }
                        }
                        else
                        {
                            Console.WriteLine("Nincs ilyen elem.");
                        }

                        Console.WriteLine("Továbblépéshez nymoja meg az ENTER-t...");
                        Console.ReadLine();
                        this.CrudMenu();
                        break;
                    } // Elem törlése
                    else if (y == 12)
                    {
                        Console.Clear();
                        try
                        {
                            Console.WriteLine(this.logic.GetAllPlayersMonster());
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("HIBA: " + e.Message);
                        }

                        Console.WriteLine("Visszalépshez nymoja meg az ENTER-t...");
                        Console.ReadLine();
                        this.CrudMenu();
                        break;
                    } // Játékosok  szörnyeinek listázása
                    else if (y == 13)
                    {
                        Console.Clear();
                        string pId;
                        int playerId;
                        string mId;
                        int monsterId;
                        do
                        {
                            Console.WriteLine("Adja meg a játékos ID-jét!");
                            pId = Console.ReadLine();
                        }
                        while (pId == string.Empty || !int.TryParse(pId, out playerId));
                        do
                        {
                            Console.WriteLine("Adja meg a szörny ID-jét!");
                            mId = Console.ReadLine();
                        }
                        while (mId == string.Empty || !int.TryParse(mId, out monsterId));
                        Console.Clear();
                        Console.WriteLine($"Rekord: {playerId} - {monsterId}");
                        Console.WriteLine("A hozzáadáshoz írjon be egy i-t.");
                        string submit = Console.ReadLine();
                        if (submit == "i")
                        {
                            try
                            {
                                this.logic.AddPlayersMonster(playerId, monsterId);
                                Console.WriteLine("Hozzáadva.");
                            }
                            catch (ArgumentException e)
                            {
                                Console.WriteLine(e.Message);
                            }
                        }
                        else
                        {
                            Console.WriteLine("Az rekord nem lett hozzáadva.");
                        }

                        Console.WriteLine("Továbblépéshez nymoja meg az ENTER-t...");
                        Console.ReadLine();
                        this.CrudMenu();
                        break;
                    } // Szörny hozzáadása egy játékoshoz
                    else if (y == 14)
                    {
                        Console.Clear();
                        string oldId;
                        int oldPlayersMonsterId;
                        do
                        {
                            Console.WriteLine("Adja meg a módosítani kívánt rekord ID-jét. (szám)");
                            oldId = Console.ReadLine();
                        }
                        while (oldId == string.Empty || !int.TryParse(oldId, out oldPlayersMonsterId));
                        Console.Clear();
                        var playersMonster = this.logic.GetPlayersMonsterById(oldPlayersMonsterId);
                        if (playersMonster != null)
                        {
                            string pId;
                            int playerId;
                            string mId;
                            int monsterId;
                            do
                            {
                                Console.WriteLine("Adja meg a játékos ID-jét!");
                                pId = Console.ReadLine();
                            }
                            while (pId == string.Empty || !int.TryParse(pId, out playerId));
                            do
                            {
                                Console.WriteLine("Adja meg a szörny ID-jét!");
                                mId = Console.ReadLine();
                            }
                            while (mId == string.Empty || !int.TryParse(mId, out monsterId));

                            var player = this.logic.GetPlayerById(playerId);
                            if (player != null)
                            {
                                var monster = this.logic.GetMonsterById(monsterId);
                                if (monster != null)
                                {
                                    Console.WriteLine("Módosítani kívánt rekord: " + playersMonster);
                                    Console.WriteLine($"Módosított rekord: {playerId} - {monsterId}");
                                    Console.WriteLine("A hozzáadáshoz írjon be egy i-t.");
                                    string submit = Console.ReadLine();
                                    if (submit == "i")
                                    {
                                        try
                                        {
                                            this.logic.UpdatePlayersMonster(oldPlayersMonsterId, playerId, monsterId);
                                            Console.WriteLine("Módosítva.");
                                        }
                                        catch (Exception e)
                                        {
                                            Console.WriteLine("HIBA: " + e.Message);
                                        }
                                    }
                                    else
                                    {
                                        Console.WriteLine("Az rekord nem lett módosítva.");
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("Nincs ilyen szörny.");
                                }
                            }
                            else
                            {
                                Console.WriteLine("Nincs ilyen játékos.");
                            }
                        }
                        else
                        {
                            Console.WriteLine("Nincs ilyen rekord.");
                        }

                        Console.WriteLine("Továbblépéshez nymoja meg az ENTER-t...");
                        Console.ReadLine();
                        this.CrudMenu();
                        break;
                    } // Játékos szörnyének módosítása
                    else if (y == 15)
                    {
                        string id;
                        int playersMonsterId;
                        Console.Clear();
                        do
                        {
                            Console.WriteLine("Adja meg a törölni kívánt rekord ID-jét. (szám)");
                            id = Console.ReadLine();
                        }
                        while (id == string.Empty || !int.TryParse(id, out playersMonsterId));
                        var playersMonster = this.logic.GetPlayersMonsterById(playersMonsterId);
                        if (playersMonster != null)
                        {
                            Console.Clear();
                            Console.WriteLine("Törölni kívánt rekord: " + playersMonster);
                            Console.WriteLine("A törléshez irjon be egy i-t.");
                            string submit = Console.ReadLine();
                            if (submit == "i")
                            {
                                try
                                {
                                    this.logic.DeletePlayersMonster(playersMonsterId);
                                    Console.WriteLine("Törölve.");
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine("HIBA: " + e.Message);
                                }
                            }
                            else
                            {
                                Console.WriteLine("A rekord nem lett törölve.");
                            }
                        }
                        else
                        {
                            Console.Clear();
                            Console.WriteLine("Nincs ilyen rekord.");
                        }

                        Console.WriteLine("Továbblépéshez nymoja meg az ENTER-t...");
                        Console.ReadLine();
                        this.CrudMenu();
                        break;
                    } // Játékos  szörnyének törlése
                    else
                    {
                        this.StartMenu();
                        break;
                    }
                }
            }
            while (!end);
        }

        private void NonCrudMenu()
        {
            Console.Clear();
            bool end = false;
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("   Egy  játékos osszes szörnyének listázása   ");
            Console.ResetColor();
            Console.WriteLine("Típusonként a játékosoknál lévő szörnyek száma");
            Console.WriteLine("            Szörnyek átlagos élete            ");
            Console.WriteLine("                    Vissza                    ");
            int y = 0;
            Console.SetCursorPosition(0, y);
            int line = 0;
            Console.CursorVisible = false;
            do
            {
                ConsoleKeyInfo key = Console.ReadKey(true);
                if (key.Key == ConsoleKey.DownArrow)
                {
                    if (y != 3)
                    {
                        y++;
                        Console.ResetColor();
                        Console.WriteLine(this.nonCrudMenu[line]);
                        Console.ForegroundColor = ConsoleColor.Blue;
                        Console.WriteLine(this.nonCrudMenu[++line]);
                        Console.SetCursorPosition(0, y);
                    }
                }
                else if (key.Key == ConsoleKey.UpArrow)
                {
                    if (y != 0)
                    {
                        y--;
                        Console.ResetColor();
                        Console.WriteLine(this.nonCrudMenu[line]);
                        Console.ForegroundColor = ConsoleColor.Blue;
                        Console.SetCursorPosition(0, y);
                        Console.WriteLine(this.nonCrudMenu[--line]);
                        Console.SetCursorPosition(0, y);
                    }
                }
                else if (key.Key == ConsoleKey.Enter)
                {
                    Console.Clear();
                    Console.ResetColor();
                    if (y == 0)
                    {
                        string id;
                        int playerId;
                        do
                        {
                            Console.WriteLine("Adja meg a játékos ID-jét. (szám)");
                            id = Console.ReadLine();
                        }
                        while (id == string.Empty || !int.TryParse(id, out playerId));
                        Console.Clear();
                        try
                        {
                            Console.WriteLine(this.logic.APlayerMonsters(playerId));
                        }
                        catch (NullReferenceException e)
                        {
                            Console.WriteLine(e.Message);
                        }

                        Console.WriteLine("Visszalépshez nymoja meg az ENTER-t...");
                        Console.ReadLine();
                        this.NonCrudMenu();
                        break;
                    } // Egy  játékos osszes szörnyének listázása
                    else if (y == 1)
                    {
                        try
                        {
                            Console.WriteLine(this.logic.NumberOfMonstersByType());
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("HIBA: " + e.Message);
                        }

                        Console.WriteLine("Visszalépshez nymoja meg az ENTER-t...");
                        Console.ReadLine();
                        this.NonCrudMenu();
                        break;
                    } // Típusonként a játékosoknál lévő szörnyek száma
                    else if (y == 2)
                    {
                        try
                        {
                            Console.WriteLine("A szörnyek átlagos élete: " + this.logic.AvarageHitpoints());
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("HIBA: " + e.Message);
                        }

                        Console.WriteLine("Visszalépshez nymoja meg az ENTER-t...");
                        Console.ReadLine();
                        this.NonCrudMenu();
                        break;
                    } // Szörnyek  átlagos élete
                    else
                    {
                        this.StartMenu();
                        break;
                    }
                }
            }
            while (!end);
        }
    }
}
