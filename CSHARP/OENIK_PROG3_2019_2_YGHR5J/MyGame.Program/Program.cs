﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyGame.Program
{
    using System;

    /// <summary>
    /// Program starts here.
    /// </summary>
    internal class Program
    {
        private static Menu menu;

        /// <summary>
        /// Program launches here.
        /// </summary>
        /// <param name="args">arguments.</param>
        public static void Main(string[] args)
        {
            menu = new Menu();
            menu.Start();
        }
    }
}
