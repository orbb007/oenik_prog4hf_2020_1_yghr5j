﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MyGame.ConsoleClient
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Waiting...");
            string url = "http://localhost:54904/api/MonstersApi/";
            using (HttpClient client = new HttpClient())
            {
                string json = client.GetStringAsync(url + "all").Result;
                Console.Clear();
                Kiiras(json);

                Console.ReadLine();
                Dictionary<string, string> postData = new Dictionary<string, string>();
                postData.Add(nameof(Monster.Name), "Bumblebee");
                postData.Add(nameof(Monster.ElementId), "1");
                postData.Add(nameof(Monster.Type), "Attack");
                postData.Add(nameof(Monster.Hitpoints), "1000");
                postData.Add(nameof(Monster.Attack), "1000");
                postData.Add(nameof(Monster.Defense), "1000");
                string response = client.PostAsync(url + "add", new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.Clear();
                Console.WriteLine("Add: " + response);
                Kiiras(json);
                Console.ReadLine();

                int monsterId = JsonConvert.DeserializeObject<List<Monster>>(json).Where(x => x.Name == " Bumblebee ").FirstOrDefault().Id;
                postData = new Dictionary<string, string>();
                postData.Add(nameof(Monster.Id), monsterId.ToString());
                postData.Add(nameof(Monster.Name), "Bumblebee");
                postData.Add(nameof(Monster.ElementId), "1");
                postData.Add(nameof(Monster.Type), "Attack");
                postData.Add(nameof(Monster.Hitpoints), "1500");
                postData.Add(nameof(Monster.Attack), "1500");
                postData.Add(nameof(Monster.Defense), "1500");

                response = client.PostAsync(url + "modify", new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.Clear();
                Console.WriteLine("Modify: " + response);
                Kiiras(json);
                Console.ReadLine();

                monsterId = JsonConvert.DeserializeObject<List<Monster>>(json).Where(x => x.Name == " Bumblebee ").FirstOrDefault().Id;
                response = client.GetStringAsync(url + "delete/" + monsterId).Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.Clear();
                Console.WriteLine("Delete: " + response);
                Kiiras(json);
                Console.ReadLine();
            }
        }
        static void Kiiras(string json)
        {
            List<Monster> list = JsonConvert.DeserializeObject<List<Monster>>(json);
            foreach (var item in list)
            {
                Console.WriteLine(item);
            }
        }
    }
}
