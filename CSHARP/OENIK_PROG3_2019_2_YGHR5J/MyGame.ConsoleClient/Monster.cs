﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyGame.ConsoleClient
{
    public class Monster
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int ElementId { get; set; }
        public string Type { get; set; }
        public int Hitpoints { get; set; }
        public int Attack { get; set; }
        public int Defense { get; set; }

        public Monster(string monster)
        {
            string[] mon = monster.Trim(' ').Split('-');
            Id = int.Parse(mon[0]);
            Name = mon[1];
            ElementId = int.Parse(mon[2]);
            Type = mon[3];
            Hitpoints = int.Parse(mon[4]);
            Attack = int.Parse(mon[5]);
            Defense = int.Parse(mon[6]);
        }
        public Monster()
        {

        }
        public override string ToString()
        {
            return $"{Id,3}{Name,15}{ElementId,10}{Type,15}{Hitpoints,10}{Attack,10}{Defense,10}";
        }
    }
}
