﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace MyGame.Web.Models
{
    public class MonstersViewModel
    {
        public List<Monster> ListOfMonsters { get; set; }
        public Monster EditedMonster { get; set; }
        public MonstersViewModel()
        {
            ListOfMonsters = new List<Monster>();
        }
    }
}