﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MyGame.Web.Models
{
    public class Monster
    {
        [Required]
        [Display(Name = "Mon Id")]
        public int Id { get; set; }
        [Required]
        [Display(Name = "Mon Name")]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Mon Element")]
        [Range(1, 8)]
        public int ElementId { get; set; }
        [Required]
        [Display(Name = "Mon Type")]
        public string Type { get; set; }
        [Required]
        [Display(Name = "Mon Hitpoints")]
        public int Hitpoints { get; set; }
        [Required]
        [Display(Name = "Mon Attack")]
        public int Attack { get; set; }
        [Required]
        [Display(Name = "Mon Defense")]
        public int Defense { get; set; }

        public Monster(string monster)
        {
            string[] mon = monster.Trim(' ').Split('-');
            Id = int.Parse(mon[0]);
            Name = mon[1];
            ElementId = int.Parse(mon[2]);
            Type = mon[3];
            Hitpoints = int.Parse(mon[4]);
            Attack = int.Parse(mon[5]);
            Defense = int.Parse(mon[6]);
        }
        public Monster()
        {

        }
    }
}