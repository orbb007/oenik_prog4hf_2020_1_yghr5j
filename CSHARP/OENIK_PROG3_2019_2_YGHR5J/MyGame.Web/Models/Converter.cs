﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace MyGame.Web.Models
{
    public static class Converter
    {
        public static List<Monster> ConvertMonsters(StringBuilder monsters)
        {
            List<Monster> ListOfMonsters = new List<Monster>();
            string[] monArray = monsters.Replace('\n', '$').ToString().Split('$');
            int i = 0;

            foreach (string item in monArray)
            {
                if (i == 0)
                {
                    i++;
                }
                else
                {
                    if (item != "")
                    {
                        ListOfMonsters.Add(new Monster(item));
                    }
                }
            }
            return ListOfMonsters;
        }
    }
}