﻿using MyGame.Logic;
using MyGame.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MyGame.Web.Controllers
{
    public class MonstersApiController : ApiController
    {
        public class ApiResult
        {
            public bool operationResult { get; set; }
        }

        GameLogic logic;

        public MonstersApiController()
        {
            this.logic = new GameLogic();
        }
        [ActionName("all")]
        [HttpGet]
        public IEnumerable<Models.Monster> GetAll()
        {
            var monsters = logic.GetAllMonster();
            return Converter.ConvertMonsters(monsters); 
        }

        [ActionName("delete")]
        [HttpGet]
        public ApiResult DeleteMonster(int id)
        {
            try
            {
                logic.DeleteMonster(id);
                return new ApiResult() { operationResult = true };
            }
            catch (Exception)
            {
                return new ApiResult() { operationResult = false };
            }
            
        }

        [ActionName("add")]
        [HttpPost]
        public ApiResult AddMonster(Monster mon)
        {
            try
            {
                logic.AddMonster(mon.Name, mon.ElementId, mon.Type, mon.Hitpoints, mon.Attack, mon.Defense);
                return new ApiResult() { operationResult = true };
            }
            catch (Exception)
            {
                return new ApiResult() { operationResult = false };
            }
        }

        [ActionName("modify")]
        [HttpPost]
        public ApiResult ModifyMonster(Monster mon)
        {
            try
            {
                logic.UpdateMonster(mon.Id, mon.Name, mon.ElementId, mon.Type, mon.Hitpoints, mon.Attack, mon.Defense);
                return new ApiResult() { operationResult = true };
            }
            catch (Exception)
            {
                return new ApiResult() { operationResult = false };
            }
        }
    }
}
