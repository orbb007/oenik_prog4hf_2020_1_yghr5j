﻿using MyGame.Logic;
using MyGame.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyGame.Web.Controllers
{
    public class MonstersController : Controller
    {
        GameLogic logic;
        MonstersViewModel vm;
        public MonstersController()
        {
            logic = new GameLogic();
            vm = new MonstersViewModel();
            vm.ListOfMonsters = Converter.ConvertMonsters(logic.GetAllMonster());
            vm.EditedMonster = new Monster();
        }
       
        public Monster GetMonster(int id)
        {
            Monster monster = new Monster(logic.GetMonsterById(id));
            return monster;
        }

        public ActionResult Index()
        {
            ViewData["editAction"] = "AddNew";
            return View("MonsterIndex", vm);
        }
        public ActionResult Details(int id)
        {
            return View("MonsterDetails", GetMonster(id));
        }
        public ActionResult Edit(int id)
        {
            ViewData["editAction"] = "edit";
            vm.EditedMonster = GetMonster(id);
            return View("MonsterIndex", vm);
        }

        [HttpPost]
        public ActionResult Edit(Monster mon, string editAction)
        {
            if (ModelState.IsValid && mon != null)
            {
                TempData["editResult"] = "Edit OK";
                if (editAction == "AddNew")
                {
                    try
                    {
                        logic.AddMonster(mon.Name, mon.ElementId, mon.Type, mon.Hitpoints, mon.Attack, mon.Defense);
                    }
                    catch (Exception)
                    {
                        TempData["editResult"] = "Addition FAIL";
                    }
                }
                else
                {
                    try
                    {
                        logic.UpdateMonster(mon.Id, mon.Name, mon.ElementId, mon.Type, mon.Hitpoints, mon.Attack, mon.Defense);
                    }
                    catch (Exception)
                    {
                        TempData["editResult"] = "Edit FAIL";
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            else
            {
                TempData["editResult"] = "Edit FAIL";
                ViewData["editAction"] = "edit";
                vm.EditedMonster = mon;
                return View("MonsterIndex", vm);
            }
        }


        public ActionResult Delete(int id)
        {
            try
            {
                TempData["editResult"] = "Delete OK";
                logic.DeleteMonster(id);
            }
            catch (Exception)
            {
                TempData["editResult"] = "Delete FAIL";
            }
            return RedirectToAction(nameof(Index));
        }

    }
}
