﻿// <copyright file="MonsterRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyGame.Repository
{
    using System.Linq;
    using MyGame.Data;

    /// <summary>
    /// Monster repository.
    /// </summary>
    public class MonsterRepository : GameRepository, IRepository<Szornyek>
    {
        /// <summary>
        /// Returns all monsters.
        /// </summary>
        /// <returns>Szornyek.</returns>
        public IQueryable<Szornyek> GetAll()
        {
            return this.Db.Szornyek;
        }

        /// <summary>
        /// Add new monster.
        /// </summary>
        /// <param name="record">Monster.</param>
        public void Add(Szornyek record)
        {
            this.Db.Szornyek.Add(record);
            this.Db.SaveChanges();
        }

        /// <summary>
        /// Delete a monster.
        /// </summary>
        /// <param name="id">Monster id.</param>
        public void Delete(int id)
        {
            this.Db.Szornyek.Remove(this.Db.Szornyek.Find(id));
            this.Db.SaveChanges();
        }

        /// <summary>
        /// Update Szornyek.
        /// </summary>
        /// <param name="oldId">Old monster id.</param>
        /// <param name="newRecord">New monster.</param>
        public void Update(int oldId, Szornyek newRecord)
        {
            this.Delete(oldId);
            this.Add(newRecord);
        }

        /// <summary>
        /// Get a monster by its id.
        /// </summary>
        /// <param name="id">Monster id.</param>
        /// <returns>Monster.</returns>
        public Szornyek GetById(int id)
        {
            return this.Db.Szornyek.Find(id);
        }
    }
}
