﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyGame.Repository
{
    using System;
    using System.Linq;

    /// <summary>
    /// Initialization interface for the Repositories.
    /// </summary>
    /// <typeparam name="T">Any type of repository.</typeparam>
    public interface IRepository<T>
    {
        /// <summary>
        /// Returns all the records of the repository.
        /// </summary>
        /// <returns>T type query.</returns>
        IQueryable<T> GetAll();

        /// <summary>
        /// Add a new record.
        /// </summary>
        /// <param name="record">Record.</param>
        void Add(T record);

        /// <summary>
        /// Deletes from the repository.
        /// </summary>
        /// <param name="id">Id.</param>
        void Delete(int id);

        /// <summary>
        /// Updates repository.
        /// </summary>
        /// <param name="oldId">Old record id.</param>
        /// <param name="newRecord">New record.</param>
        void Update(int oldId, T newRecord);

        /// <summary>
        ///  Get a record by its id.
        /// </summary>
        /// <param name="id">Id.</param>
        /// <returns>Any type of repository.</returns>
        T GetById(int id);
    }
}
