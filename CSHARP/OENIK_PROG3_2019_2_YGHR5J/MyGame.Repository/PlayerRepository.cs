﻿// <copyright file="PlayerRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyGame.Repository
{
    using System.Linq;
    using MyGame.Data;

    /// <summary>
    /// Player repository.
    /// </summary>
    public class PlayerRepository : GameRepository, IRepository<Jatekosok>
    {
        /// <summary>
        /// Returns all players.
        /// </summary>
        /// <returns>Jatekosok.</returns>
        public IQueryable<Jatekosok> GetAll()
        {
            return this.Db.Jatekosok;
        }

        /// <summary>
        /// Add new player.
        /// </summary>
        /// <param name="record">Player.</param>
        public void Add(Jatekosok record)
        {
            this.Db.Jatekosok.Add(record);
            this.Db.SaveChanges();
        }

        /// <summary>
        /// Delete a player by its id.
        /// </summary>
        /// <param name="id">Player id.</param>
        public void Delete(int id)
        {
            this.Db.Jatekosok.Remove(this.Db.Jatekosok.Find(id));
            this.Db.SaveChanges();
        }

        /// <summary>
        /// Update Players.
        /// </summary>
        /// <param name="oldId">Old player id.</param>
        /// <param name="newRecord">New player.</param>
        public void Update(int oldId, Jatekosok newRecord)
        {
            this.Delete(oldId);
            this.Add(newRecord);
        }

        /// <summary>
        /// Get a player by its id.
        /// </summary>
        /// <param name="id">Player id.</param>
        /// <returns>Player.</returns>
        public Jatekosok GetById(int id)
        {
            return this.Db.Jatekosok.Find(id);
        }
    }
}
