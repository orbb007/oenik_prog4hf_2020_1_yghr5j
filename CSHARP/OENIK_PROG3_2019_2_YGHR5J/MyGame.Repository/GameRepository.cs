﻿// <copyright file="GameRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyGame.Repository
{
    using MyGame.Data;

    /// <summary>
    /// Game Repository.
    /// </summary>
    public class GameRepository
    {
        /// <summary>
        /// Gets game database.
        /// </summary>
        public GameDbEntities Db = new GameDbEntities();
    }
}
