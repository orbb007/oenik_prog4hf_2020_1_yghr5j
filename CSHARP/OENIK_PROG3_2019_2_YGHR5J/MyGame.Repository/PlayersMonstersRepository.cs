﻿// <copyright file="PlayersMonstersRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyGame.Repository
{
    using System.Linq;
    using MyGame.Data;

    /// <summary>
    /// Player's monsters repository.
    /// </summary>
    public class PlayersMonstersRepository : GameRepository, IRepository<JatekosSzornyKapcsolo>
    {
        /// <summary>
        /// Returns all player's monsters.
        /// </summary>
        /// <returns>Player's monsters.</returns>
        public IQueryable<JatekosSzornyKapcsolo> GetAll()
        {
            return this.Db.JatekosSzornyKapcsolo;
        }

        /// <summary>
        /// Add new monster to a player.
        /// </summary>
        /// <param name="record">Player's monster.</param>
        public void Add(JatekosSzornyKapcsolo record)
        {
            this.Db.JatekosSzornyKapcsolo.Add(record);
            this.Db.SaveChanges();
        }

        /// <summary>
        /// Delete a player's monster.
        /// </summary>
        /// <param name="id">Player's monster id.</param>
        public void Delete(int id)
        {
            this.Db.JatekosSzornyKapcsolo.Remove(this.Db.JatekosSzornyKapcsolo.Find(id));
            this.Db.SaveChanges();
        }

        /// <summary>
        /// Update player's monsters.
        /// </summary>
        /// <param name="oldId">Old player's monster id.</param>
        /// <param name="newRecord">New player's monster.</param>
        public void Update(int oldId, JatekosSzornyKapcsolo newRecord)
        {
            this.Delete(oldId);
            this.Add(newRecord);
        }

        /// <summary>
        /// Get a player's monster by its id.
        /// </summary>
        /// <param name="id">Player's monster id.</param>
        /// <returns>Player's monster.</returns>
        public JatekosSzornyKapcsolo GetById(int id)
        {
            return this.Db.JatekosSzornyKapcsolo.Find(id);
        }
    }
}