﻿// <copyright file="ElementRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyGame.Repository
{
    using System.Linq;
    using MyGame.Data;

    /// <summary>
    /// Element repository.
    /// </summary>
    public class ElementRepository : GameRepository, IRepository<Elemek>
    {
        /// <summary>
        /// Returns all elements.
        /// </summary>
        /// <returns>Jatekosok.</returns>
        public IQueryable<Elemek> GetAll()
        {
            return this.Db.Elemek;
        }

        /// <summary>
        /// Add new element.
        /// </summary>
        /// <param name="record">Element.</param>
        public void Add(Elemek record)
        {
            this.Db.Elemek.Add(record);
            this.Db.SaveChanges();
        }

        /// <summary>
        /// Delete an element.
        /// </summary>
        /// <param name="id">Element id.</param>
        public void Delete(int id)
        {
            this.Db.Elemek.Remove(this.Db.Elemek.Find(id));
            this.Db.SaveChanges();
        }

        /// <summary>
        /// Update elements.
        /// </summary>
        /// <param name="oldId">Old element id.</param>
        /// <param name="newRecord">New element.</param>
        public void Update(int oldId, Elemek newRecord)
        {
            this.Delete(oldId);
            this.Add(newRecord);
        }

        /// <summary>
        /// Get a element by its id.
        /// </summary>
        /// <param name="id">Element id.</param>
        /// <returns>Element.</returns>
        public Elemek GetById(int id)
        {
            return this.Db.Elemek.Find(id);
        }
    }
}