﻿// <copyright file="LogicTester.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyGame.Logic.Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Moq;
    using MyGame.Data;
    using MyGame.Repository;
    using NUnit.Framework;

    /// <summary>
    /// Test.
    /// </summary>
    [TestFixture]
    public class LogicTester
    {
        private GameLogic logic;

        /// <summary>
        /// Setup.
        /// </summary>
        [SetUp]
        public void Init()
        {
            var playersMock = new Mock<IRepository<Jatekosok>>();
            var monstersMock = new Mock<IRepository<Szornyek>>();
            var elementsMock = new Mock<IRepository<Elemek>>();
            var playersMonstersMock = new Mock<IRepository<JatekosSzornyKapcsolo>>();

            List<Jatekosok> players = new List<Jatekosok>();
            List<Szornyek> monsters = new List<Szornyek>();
            List<Elemek> elements = new List<Elemek>();
            List<JatekosSzornyKapcsolo> playersMonsters = new List<JatekosSzornyKapcsolo>();

            players.Add(new Jatekosok()
            {
                jatekosId = 1,
                jatekosNev = "Alma",
                kezdesDatuma = "2015.04.01",
                szint = 65,
                klan = "Summoners",
                rang = "Arany",
            });
            players.Add(new Jatekosok()
            {
                jatekosId = 2,
                jatekosNev = "Manooth",
                kezdesDatuma = "2016.03.03",
                szint = 75,
                klan = "Summoners",
                rang = "Arany",
            });
            players.Add(new Jatekosok()
            {
                jatekosId = 3,
                jatekosNev = "Izso",
                kezdesDatuma = "2015.02.14",
                szint = 50,
                klan = "ProcAndRoll",
                rang = "Ezust",
            });

            monsters.Add(new Szornyek()
            {
                szornyId = 1,
                szornyNev = "Rakan",
                elemId = 1,
                tipus = "elet",
                elet = 11700,
                tamadas = 725,
                vedelem = 637,
            });
            monsters.Add(new Szornyek()
            {
                szornyId = 2,
                szornyNev = "Zeratu",
                elemId = 3,
                tipus = "tamadas",
                elet = 10710,
                tamadas = 790,
                vedelem = 637,
            });
            monsters.Add(new Szornyek()
            {
                szornyId = 3,
                szornyNev = "Theo",
                elemId = 2,
                tipus = "tamadas",
                elet = 10865,
                tamadas = 823,
                vedelem = 593,
            });

            elements.Add(new Elemek()
            {
                elemId = 1,
                elemNev = "Tuz",
                erosseg = 3,
                gyengeseg = 2,
                ritkasag = "gyakori",
                szin = "piros",
            });
            elements.Add(new Elemek()
            {
                elemId = 2,
                elemNev = "Viz",
                erosseg = 1,
                gyengeseg = 3,
                ritkasag = "gyakori",
                szin = "sotet kek",
            });
            elements.Add(new Elemek()
            {
                elemId = 3,
                elemNev = "szel",
                erosseg = 2,
                gyengeseg = 1,
                ritkasag = "gyakori",
                szin = "sarga",
            });

            playersMonsters.Add(new JatekosSzornyKapcsolo()
            {
                kapcsoloId = 1,
                jatekosId = 1,
                szornyId = 1,
            });
            playersMonsters.Add(new JatekosSzornyKapcsolo()
            {
                kapcsoloId = 2,
                jatekosId = 1,
                szornyId = 2,
            });
            playersMonsters.Add(new JatekosSzornyKapcsolo()
            {
                kapcsoloId = 3,
                jatekosId = 1,
                szornyId = 3,
            });
            playersMonsters.Add(new JatekosSzornyKapcsolo()
            {
                kapcsoloId = 4,
                jatekosId = 2,
                szornyId = 3,
            });
            playersMonsters.Add(new JatekosSzornyKapcsolo()
            {
                kapcsoloId = 5,
                jatekosId = 3,
                szornyId = 2,
            });
            playersMonsters.Add(new JatekosSzornyKapcsolo()
            {
                kapcsoloId = 6,
                jatekosId = 3,
                szornyId = 3,
            });

            playersMock.Setup(m => m.GetAll()).Returns(players.AsQueryable());
            playersMock.Setup(m => m.GetById(It.IsAny<int>())).Returns((int i)
                => players.Where(x => x.jatekosId == i).SingleOrDefault());

            monstersMock.Setup(m => m.GetAll()).Returns(monsters.AsQueryable());
            monstersMock.Setup(m => m.GetById(It.IsAny<int>())).Returns((int i)
                => monsters.Where(x => x.szornyId == i).SingleOrDefault());

            elementsMock.Setup(m => m.GetAll()).Returns(elements.AsQueryable());
            elementsMock.Setup(m => m.GetById(It.IsAny<int>())).Returns((int i)
                => elements.Where(x => x.elemId == i).SingleOrDefault());

            playersMonstersMock.Setup(m => m.GetAll()).Returns(playersMonsters.AsQueryable());
            playersMonstersMock.Setup(m => m.GetById(It.IsAny<int>())).Returns((int i)
                => playersMonsters.Where(x => x.kapcsoloId == i).SingleOrDefault());

            this.logic = new GameLogic(playersMock.Object, monstersMock.Object, elementsMock.Object, playersMonstersMock.Object);
        }

        /// <summary>
        /// Insert test.
        /// </summary>
        [Test]
        public void InsertTest()
        {
            Assert.That(
            () => this.logic.AddElement("Jeg", 2, 4, "ritka", "vilagos kek"),
            Throws.TypeOf<ArgumentException>());
        }

        /// <summary>
        /// Delete test.
        /// </summary>
        [Test]
        public void DeleteTest()
        {
            Assert.That(() => this.logic.DeleteMonster(4), Throws.TypeOf<NullReferenceException>());
        }

        /// <summary>
        /// Update test.
        /// </summary>
        [Test]
        public void UpdateTest()
        {
            Assert.That(() => this.logic.UpdatePlayersMonster(1, 1, 4), Throws.TypeOf<ArgumentException>());
        }

        /// <summary>
        /// Get by id test.
        /// </summary>
        [Test]
        public void GetByIdTest()
        {
            Assert.That(this.logic.GetPlayerById(4) == null);
            Assert.That(this.logic.GetPlayerById(1).Length > 0);
            Assert.That(this.logic.GetPlayerById(1) == "1 - Alma - 2015.04.01 - 65 - Summoners - Arany");
        }

        /// <summary>
        /// Get all test.
        /// </summary>
        [Test]
        public void GetAllTest()
        {
            Assert.That(this.logic.GetAllPlayer().Length > 0);
        }

        /// <summary>
        /// Player monsters test.
        /// </summary>
        [Test]
        public void APlayerMonstersTest()
        {
            StringBuilder mons = this.logic.APlayerMonsters(1);
            Assert.That(mons.Length > 0);
            Assert.That(() => this.logic.APlayerMonsters(4), Throws.TypeOf<NullReferenceException>());
        }

        /// <summary>
        /// Number of monsters test.
        /// </summary>
        [Test]
        public void NumberOfMonstersByTypeTest()
        {
            StringBuilder sb = this.logic.NumberOfMonstersByType();
            StringBuilder sb2 = new StringBuilder();
            sb2.Append("elet - 1");
            sb2.AppendLine();
            sb2.Append("tamadas - 5");
            sb2.AppendLine();

            Assert.That(sb.Length > 0);
            Assert.That(sb.ToString() == sb2.ToString());
        }

        /// <summary>
        /// Avarage monster hp.
        /// </summary>
        [Test]
        public void AvarageHitpointsTest()
        {
            double avg = this.logic.AvarageHitpoints();
            Assert.That(avg == 11091);
        }
    }
}
