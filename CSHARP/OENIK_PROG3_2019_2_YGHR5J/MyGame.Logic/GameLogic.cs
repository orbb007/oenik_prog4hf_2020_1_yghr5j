﻿// <copyright file="GameLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyGame.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Threading.Tasks;
    using MyGame.Data;
    using MyGame.Repository;

    /// <summary>
    /// Game logic.
    /// </summary>
    public class GameLogic : ILogic
    {
        private IRepository<Jatekosok> playerRepo;
        private IRepository<Szornyek> monsterRepo;
        private IRepository<Elemek> elementRepo;
        private IRepository<JatekosSzornyKapcsolo> playersMonstersRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameLogic"/> class.
        /// </summary>
        public GameLogic()
        {
            this.playerRepo = new PlayerRepository();
            this.monsterRepo = new MonsterRepository();
            this.elementRepo = new ElementRepository();
            this.playersMonstersRepo = new PlayersMonstersRepository();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GameLogic"/> class.
        /// </summary>
        /// <param name="p">Jatekosok repo.</param>
        /// <param name="m">Szornyek repo.</param>
        /// <param name="e">Elemek repo.</param>
        /// <param name="pm">JatekosSzornyKapcsolo repo.</param>
        public GameLogic(IRepository<Jatekosok> p, IRepository<Szornyek> m, IRepository<Elemek> e, IRepository<JatekosSzornyKapcsolo> pm)
        {
            this.playerRepo = p;
            this.monsterRepo = m;
            this.elementRepo = e;
            this.playersMonstersRepo = pm;
        }

        /// <inheritdoc/>
        public StringBuilder GetAllElement()
        {
            StringBuilder sb = new StringBuilder();
            List<Elemek> elements = this.elementRepo.GetAll().ToList();
            sb.Append("(id,  név,  erősség(elem id),  gyengeség(elem id),  ritkaság,  szín)\n");
            foreach (var element in elements)
            {
                sb.Append(element.elemId + " - " + element.elemNev + " - " + element.erosseg + " - " + element.gyengeseg + " - " +
                    element.ritkasag + " - " + element.szin);
                sb.Append("\n");
            }

            return sb;
        }

        /// <inheritdoc/>
        public StringBuilder GetAllMonster()
        {
            StringBuilder sb = new StringBuilder();
            List<Szornyek> monsters = this.monsterRepo.GetAll().ToList();
            sb.Append("(id,  név,  elem id,  típus,  élet,  támadás,  védelem)\n");
            foreach (var monster in monsters)
            {
                sb.Append(monster.szornyId + " - " + monster.szornyNev + " - " + monster.elemId + " - " + monster.tipus + " - " +
                    monster.elet + " - " + monster.tamadas + " - " + monster.vedelem);
                sb.Append("\n");
            }

            return sb;
            //return this.monsterRepo.GetAll();
        }

        /// <inheritdoc/>
        public StringBuilder GetAllPlayer()
        {
            StringBuilder sb = new StringBuilder();
            List<Jatekosok> players = this.playerRepo.GetAll().ToList();
            sb.Append("(id,  név,  kezdés dátuma,  szint,  klán,  rang)\n");
            foreach (var player in players)
            {
                sb.Append(player.jatekosId + " - " + player.jatekosNev + " - " + player.kezdesDatuma + " - " + player.szint + " - " + player.klan + " - " + player.rang);
                sb.Append("\n");
            }

            return sb;
        }

        /// <inheritdoc/>
        public StringBuilder GetAllPlayersMonster()
        {
            StringBuilder sb = new StringBuilder();
            List<JatekosSzornyKapcsolo> playersMonsters = this.playersMonstersRepo.GetAll().ToList();
            sb.Append("(id,  jatekos id,  szörny id)\n");
            foreach (var playersMonster in playersMonsters)
            {
                sb.Append(playersMonster.kapcsoloId + " - " + playersMonster.jatekosId + " - " + playersMonster.szornyId);
                sb.Append("\n");
            }

            return sb;
        }

        /// <inheritdoc/>
        public void AddElement(string name, int strength, int weakness, string rarity, string color)
        {
            if (this.GetElementById(strength) != null && this.GetElementById(weakness) != null)
            {
                Elemek element = new Elemek()
                {
                    elemNev = name,
                    erosseg = strength,
                    gyengeseg = weakness,
                    ritkasag = rarity,
                    szin = color,
                };
                this.elementRepo.Add(element);
            }
            else
            {
                throw new ArgumentException("A hivatkozás nem megfelelő");
            }
        }

        /// <inheritdoc/>
        public void AddMonster(string name, int elementId, string type, int hitPoints, int attack, int defense)
        {
            if (this.GetElementById(elementId) != null)
            {
                Szornyek monster = new Szornyek()
                {
                    szornyNev = name,
                    elemId = elementId,
                    tipus = type,
                    elet = hitPoints,
                    tamadas = attack,
                    vedelem = defense,
                };
                this.monsterRepo.Add(monster);
            }
            else
            {
                throw new ArgumentException("A hivatkozás nem megfelelő");
            }
        }

        /// <inheritdoc/>
        public void AddPlayer(string name, string startDate, int level, string guild, string rank)
        {
            Jatekosok player = new Jatekosok()
            {
                jatekosNev = name,
                kezdesDatuma = startDate,
                szint = level,
                klan = guild,
                rang = rank,
            };
            this.playerRepo.Add(player);
        }

        /// <inheritdoc/>
        public void AddPlayersMonster(int playerId, int monsterId)
        {
            if (this.GetPlayerById(playerId) != null && this.GetMonsterById(monsterId) != null)
            {
                JatekosSzornyKapcsolo playersMonster = new JatekosSzornyKapcsolo()
                {
                    jatekosId = playerId,
                    szornyId = monsterId,
                };
                this.playersMonstersRepo.Add(playersMonster);
            }
            else
            {
                throw new ArgumentException("A hivatkozás nem megfelelő");
            }
        }

        /// <inheritdoc/>
        public void DeleteElement(int id)
        {
            if (this.GetElementById(id) != null)
            {
                this.elementRepo.Delete(id);
            }
            else
            {
                throw new NullReferenceException("Nincs ilyen elem.");
            }
        }

        /// <inheritdoc/>
        public void DeleteMonster(int id)
        {
            if (this.GetMonsterById(id) != null)
            {
                this.monsterRepo.Delete(id);
            }
            else
            {
                throw new NullReferenceException("Nincs ilyen szörny.");
            }
        }

        /// <inheritdoc/>
        public void DeletePlayer(int id)
        {
            if (this.GetPlayerById(id) != null)
            {
                this.playerRepo.Delete(id);
            }
            else
            {
                throw new NullReferenceException("Nincs ilyen játékos.");
            }
        }

        /// <inheritdoc/>
        public void DeletePlayersMonster(int id)
        {
            if (this.GetPlayersMonsterById(id) != null)
            {
                this.playersMonstersRepo.Delete(id);
            }
            else
            {
                throw new NullReferenceException("Nincs ilyen rekord.");
            }
        }

        /// <inheritdoc/>
        public void UpdatePlayer(int oldId, string newName, string newStartDate, int newLevel, string newGuild, string newRank)
        {
            this.DeletePlayer(oldId);
            this.AddPlayer(newName, newStartDate, newLevel, newGuild, newRank);
        }

        /// <inheritdoc/>
        public void UpdateMonster(int oldId, string newName, int newElementId, string newType, int newHitPoints, int newAttack, int newDefense)
        {
            this.DeleteMonster(oldId);
            this.AddMonster(newName, newElementId, newType, newHitPoints, newAttack, newDefense);
        }

        /// <inheritdoc/>
        public void UpdateElement(int oldId, string newName, int newStrength, int newWeakness, string newRarity, string newColor)
        {
            this.DeleteElement(oldId);
            this.AddElement(newName, newStrength, newWeakness, newRarity, newColor);
        }

        /// <inheritdoc/>
        public void UpdatePlayersMonster(int oldId, int newPlayerId, int newMonsterId)
        {
            this.DeletePlayersMonster(oldId);
            this.AddPlayersMonster(newPlayerId, newMonsterId);
        }

        /// <inheritdoc/>
        public string GetPlayerById(int id)
        {
            Jatekosok player = this.playerRepo.GetById(id);
            if (player != null)
            {
                return $"{player.jatekosId} - {player.jatekosNev} - {player.kezdesDatuma} - {player.szint} - {player.klan} - {player.rang}";
            }
            else
            {
                return null;
            }
        }

        /// <inheritdoc/>
        public string GetMonsterById(int id)
        {
            Szornyek monster = this.monsterRepo.GetById(id);
            if (monster != null)
            {
                return $"{monster.szornyId} - {monster.szornyNev} - {monster.elemId} - {monster.tipus} - {monster.elet} - {monster.tamadas} - {monster.vedelem}";
            }
            else
            {
                return null;
            }
        }

        /// <inheritdoc/>
        public string GetElementById(int id)
        {
            Elemek element = this.elementRepo.GetById(id);
            if (element != null)
            {
                return $"{element.elemId} - {element.elemNev} - {element.erosseg} - {element.gyengeseg} - {element.ritkasag} - {element.szin}";
            }
            else
            {
                return null;
            }
        }

        /// <inheritdoc/>
        public string GetPlayersMonsterById(int id)
        {
            JatekosSzornyKapcsolo playersMonster = this.playersMonstersRepo.GetById(id);
            if (playersMonster != null)
            {
                return $"{playersMonster.kapcsoloId} - {playersMonster.jatekosId} - {playersMonster.szornyId}";
            }
            else
            {
                return null;
            }
        }

        /// <inheritdoc/>
        public StringBuilder APlayerMonsters(int id)
        {
            StringBuilder sb = new StringBuilder();
            IQueryable<Szornyek> monsters = this.monsterRepo.GetAll();
            IQueryable<JatekosSzornyKapcsolo> playersMonsters = this.playersMonstersRepo.GetAll();
            if (this.GetPlayerById(id) != null)
            {
                var q = from x in monsters.ToList()
                        join y in playersMonsters.ToList() on x.szornyId equals y.szornyId
                        where y.jatekosId == id
                        select new { x.szornyNev };

                foreach (var item in q)
                {
                    sb.Append(item.szornyNev);
                    sb.AppendLine();
                }

                return sb;
            }
            else
            {
                throw new NullReferenceException("Nincs ilyen játékos.");
            }
        }

        /// <inheritdoc/>
        public StringBuilder NumberOfMonstersByType()
        {
            StringBuilder sb = new StringBuilder();
            IQueryable<Szornyek> monsters = this.monsterRepo.GetAll();
            IQueryable<JatekosSzornyKapcsolo> playersMonsters = this.playersMonstersRepo.GetAll();

            var q = from x in playersMonsters.ToList()
                    join y in monsters.ToList() on x.szornyId equals y.szornyId
                    group x by y.tipus into g
                    select new
                    {
                        TYPE = g.Key,
                        COUNT = g.Count(),
                    };

            foreach (var item in q)
            {
                sb.Append(item.TYPE + " - " + item.COUNT);
                sb.AppendLine();
            }

            return sb;
        }

        /// <inheritdoc/>
        public int AvarageHitpoints()
        {
            IQueryable<Szornyek> monsters = this.monsterRepo.GetAll();
            return (int)monsters.Average(t => t.elet);
        }

        /// <inheritdoc/>
        public void RandomMonsterToAPlayer(int id)
        {
            if (this.playerRepo.GetById(id) != null)
            {
                WebClient client = new WebClient();
                List<int> ids = new List<int>();
                var monsters = this.monsterRepo.GetAll().ToList();
                for (int i = 0; i < monsters.Count; i++)
                {
                    ids.Add(monsters[i].szornyId);
                }

                int min = this.monsterRepo.GetAll().Min(t => t.szornyId);
                int max = this.monsterRepo.GetAll().Max(t => t.szornyId);

                client.Encoding = Encoding.UTF8;
                string response = "  ";
                do
                {
                    response = client.DownloadString(@"http://localhost:8080/MyGame/?monsterId=" + min + "-" + max);
                }
                while (!ids.Contains(int.Parse(response.Split(':')[1].Split('}')[0])));

                this.playersMonstersRepo.Add(new JatekosSzornyKapcsolo()
                {
                    jatekosId = id,
                    szornyId = int.Parse(response.Split(':')[1].Split('}')[0]),
                });
            }
            else
            {
                throw new NullReferenceException("Nincs ilyen játékos.");
            }
        }
    }
}
