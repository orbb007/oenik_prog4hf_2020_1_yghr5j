﻿// <copyright file="ILogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyGame.Logic
{
    using System.Linq;
    using System.Text;
    using MyGame.Data;

    /// <summary>
    /// Logic interface.
    /// </summary>
    internal interface ILogic
    {
        /// <summary>
        /// Get all players.
        /// </summary>
        /// <returns>String.</returns>
        StringBuilder GetAllPlayer();

        /// <summary>
        /// Get all monsters.
        /// </summary>
        /// <returns>String.</returns>
        StringBuilder GetAllMonster();

        /// <summary>
        /// Get all elements.
        /// </summary>
        /// <returns>String.</returns>
        StringBuilder GetAllElement();

        /// <summary>
        /// Get all player's monsters.
        /// </summary>
        /// <returns>String.</returns>
        StringBuilder GetAllPlayersMonster();

        /// <summary>
        /// Add a new player.
        /// </summary>
        /// <param name="name">Name.</param>
        /// <param name="startDate">Start date.</param>
        /// <param name="level">Level.</param>
        /// <param name="guild">Guild.</param>
        /// <param name="rank">Rang.</param>
        void AddPlayer(string name, string startDate, int level, string guild, string rank);

        /// <summary>
        /// Add a new monster.
        /// </summary>
        /// <param name="name">Name.</param>
        /// <param name="elementId">Element id.</param>
        /// <param name="type">Type.</param>
        /// <param name="hitPoints">Hitpoints.</param>
        /// <param name="attack">Attack.</param>
        /// <param name="defense">Defense.</param>
        void AddMonster(string name, int elementId, string type, int hitPoints, int attack, int defense);

        /// <summary>
        /// Add a new element.
        /// </summary>
        /// <param name="name">Name.</param>
        /// <param name="strength">Strength.</param>
        /// <param name="weakness">Weakness.</param>
        /// <param name="rarity">Rarity.</param>
        /// <param name="color">Color.</param>
        void AddElement(string name, int strength, int weakness, string rarity, string color);

        /// <summary>
        /// Add a new monster to a player.
        /// </summary>
        /// <param name="playerId">Player id.</param>
        /// <param name="monsterId">Monster id.</param>
        void AddPlayersMonster(int playerId, int monsterId);

        /// <summary>
        /// Delete a player.
        /// </summary>
        /// <param name="id">Player id.</param>
        void DeletePlayer(int id);

        /// <summary>
        /// Delete a monster.
        /// </summary>
        /// <param name="id">Monster id.</param>
        void DeleteMonster(int id);

        /// <summary>
        /// Delete a element.
        /// </summary>
        /// <param name="id">Element id.</param>
        void DeleteElement(int id);

        /// <summary>
        /// Delete a player's monster.
        /// </summary>
        /// <param name="id">Player's monster id.</param>
        void DeletePlayersMonster(int id);

        /// <summary>
        /// Updates players.
        /// </summary>
        /// <param name="oldId">Old id.</param>
        /// <param name="newName">New name.</param>
        /// <param name="newStartDate">New start date.</param>
        /// <param name="newLevel">New level.</param>
        /// <param name="newGuild">New guild.</param>
        /// <param name="newRank">New rank.</param>
        void UpdatePlayer(int oldId, string newName, string newStartDate, int newLevel, string newGuild, string newRank);

        /// <summary>
        /// Updates monsters.
        /// </summary>
        /// <param name="oldId">Old id.</param>
        /// <param name="newName">New name.</param>
        /// <param name="newElementId">New element id.</param>
        /// <param name="newType">New type.</param>
        /// <param name="newHitPoints">New hitpoints.</param>
        /// <param name="newAttack">New attack.</param>
        /// <param name="newDefense">New defense.</param>
        void UpdateMonster(int oldId, string newName, int newElementId, string newType, int newHitPoints, int newAttack, int newDefense);

        /// <summary>
        /// Upadtes elements.
        /// </summary>
        /// <param name="oldId">Old id.</param>
        /// <param name="newName">New name.</param>
        /// <param name="newStrength">New strength.</param>
        /// <param name="newWeakness">New weakness.</param>
        /// <param name="newRarity">New rarity.</param>
        /// <param name="newColor">New color.</param>
        void UpdateElement(int oldId, string newName, int newStrength, int newWeakness, string newRarity, string newColor);

        /// <summary>
        /// Updates player's monsters.
        /// </summary>
        /// <param name="oldId">Old id.</param>
        /// <param name="newPlayerId">New player id.</param>
        /// <param name="newMonsterId">New monster id.</param>
        void UpdatePlayersMonster(int oldId, int newPlayerId, int newMonsterId);

        /// <summary>
        /// Get a player by its id.
        /// </summary>
        /// <param name="id">Player id.</param>
        /// <returns>String.</returns>
        string GetPlayerById(int id);

        /// <summary>
        /// Get a mosnter by its id.
        /// </summary>
        /// <param name="id">Monster id.</param>
        /// <returns>String.</returns>
        string GetMonsterById(int id);

        /// <summary>
        /// Get a element by its id.
        /// </summary>
        /// <param name="id">Element id.</param>
        /// <returns>String.</returns>
        string GetElementById(int id);

        /// <summary>
        /// Get a player's monster by its id.
        /// </summary>
        /// <param name="id">Player's monster id.</param>
        /// <returns>String.</returns>
        string GetPlayersMonsterById(int id);

        /// <summary>
        /// Get a player's monsters.
        /// </summary>
        /// <param name="id">Player id.</param>
        /// <returns>String.</returns>
        StringBuilder APlayerMonsters(int id);

        /// <summary>
        /// Get the number of monsters by type.
        /// </summary>
        /// <returns>String.</returns>
        StringBuilder NumberOfMonstersByType();

        /// <summary>
        /// Get the monsters avarage stats.
        /// </summary>
        /// <returns>String.</returns>
        int AvarageHitpoints();

        /// <summary>
        /// Give a random monster to a player.
        /// </summary>
        /// <param name="id">Player's id.</param>
        void RandomMonsterToAPlayer(int id);
    }
}
