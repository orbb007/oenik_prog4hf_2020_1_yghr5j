IF OBJECT_ID('Jatekosok', 'U') IS NOT NULL DROP TABLE Jatekosok;
 
CREATE TABLE Jatekosok (
    jatekosId INT IDENTITY(1,1),
    jatekosNev VARCHAR(20) NOT NULL,
    kezdesDatuma VARCHAR(10) NOT NULL,
    szint INT NOT NULL,
    klan VARCHAR(30),
    rang VARCHAR(10),
    CONSTRAINT JATEKOSOK_PRIMARY_KEY PRIMARY KEY (jatekosId));
   

INSERT INTO Jatekosok  VALUES ('Bumble', '2015.04.01', 99, 'ProcAndRoll', 'Gyemant');
INSERT INTO Jatekosok  VALUES ('Manooth', '2016.03.03', 65, 'Summoners', 'Arany');
INSERT INTO Jatekosok  VALUES ('Izso', '2015.02.14', 75, 'ProcAndRoll', 'Arany');
INSERT INTO Jatekosok  VALUES ('Exodu', '2018.12.13', 50, 'Summoners', 'Ezust');
INSERT INTO Jatekosok  VALUES ('Natszukao', '2018.01.01', 56, 'Fighters', 'Ezust');
INSERT INTO Jatekosok (jatekosNev, kezdesDatuma, szint, klan) VALUES ('Bubicod', '2019.03.03', 10, 'Fighters');
INSERT INTO Jatekosok  VALUES ('Nyokki', '2016.08.25', 84, 'ProcAndRoll', 'Arany');
INSERT INTO Jatekosok  VALUES ('Nyak', '2019.05.23', 23, 'Fighters', 'Bronz');
INSERT INTO Jatekosok  VALUES ('Kerko', '2015.06.01', 40, 'ProcAndRoll', 'Arany');
INSERT INTO Jatekosok (jatekosNev, kezdesDatuma, szint, rang) VALUES ('Pixi', '2018.04.02', 66, 'Ezust');
INSERT INTO Jatekosok  VALUES ('Dom', '2019.10.19', 5, 'Fighters', 'Bronz');
INSERT INTO Jatekosok  VALUES ('Ramul', '2016.07.19', 50, 'Summoners', 'Ezust');
INSERT INTO Jatekosok  VALUES ('Leeds', '2017.05.21', 19, 'Fighters', 'Bronz');
INSERT INTO Jatekosok (jatekosNev, kezdesDatuma, szint, klan) VALUES ('Legan', '2017.01.31', 20, 'Fighters');
INSERT INTO Jatekosok (jatekosNev, kezdesDatuma, szint, rang) VALUES ('Lulu', '2018.04.26', 55, 'Ezust');
INSERT INTO Jatekosok  VALUES ('Radur', '2016.11.06', 22, 'Summoners', 'Bronz');

IF OBJECT_ID('Elemek', 'U') IS NOT NULL DROP TABLE Elemek;
CREATE TABLE Elemek (
    elemId INT IDENTITY(1,1) NOT NULL,
    elemNev VARCHAR(20) NOT NULL,
    erosseg INT,
    gyengeseg INT,
    ritkasag VARCHAR(20) NOT NULL,
    szin VARCHAR(20) NOT NULL,
    CONSTRAINT ELEMEK_PRIMARY_KEY PRIMARY KEY (elemId));

INSERT INTO Elemek VALUES ('Tuz', 3, 2, 'gyakori', 'piros');
INSERT INTO Elemek VALUES ('Viz', 1, 3, 'gyakori', 'sotet kek');
INSERT INTO Elemek VALUES ('Szel', 2, 1, 'gyakori', 'sarga');
INSERT INTO Elemek VALUES ('Villam', 6, 5, 'ritka', 'kek');
INSERT INTO Elemek VALUES ('Jeg', 4, 6, 'ritka', 'vilagos kek');
INSERT INTO Elemek VALUES ('Fold', 5, 4, 'ritka', 'barna');
INSERT INTO Elemek (elemNev, erosseg, ritkasag, szin) VALUES ('Sotetseg', 8, 'nagyon ritka', 'sotet lila');
INSERT INTO Elemek (elemNev, erosseg, ritkasag, szin) VALUES ('Vilagossag', 7, 'nagyon ritka', 'feher');

ALTER TABLE Elemek ADD CONSTRAINT Elemek_erosseg_fk FOREIGN KEY (erosseg) REFERENCES Elemek(elemId) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE Elemek ADD CONSTRAINT Elemek_gyengeseg_fk FOREIGN KEY (gyengeseg) REFERENCES Elemek(elemId) ON DELETE NO ACTION ON UPDATE NO ACTION;


IF OBJECT_ID('Szornyek', 'U') IS NOT NULL DROP TABLE Szornyek;
CREATE TABLE Szornyek (
    szornyId INT IDENTITY(1,1) NOT NULL,
    szornyNev VARCHAR(20) NOT NULL,
    elemId INT NOT NULL REFERENCES Elemek(elemId) ON DELETE CASCADE ON UPDATE CASCADE,
    tipus VARCHAR(20) NOT NULL,
    elet INT NOT NULL,
    tamadas INT NOT NULL,
    vedelem INT NOT NULL
    CONSTRAINT SZORNYEK_PRIMARY_KEY PRIMARY KEY (szornyId));


INSERT INTO Szornyek VALUES ('Rakan', 1, 'elet', 11700, 725, 637);
INSERT INTO Szornyek VALUES ('Zeratu', 7, 'tamadas', 10710, 790, 637);
INSERT INTO Szornyek VALUES ('Theo', 5, 'tamadas', 10875, 823, 593);
INSERT INTO Szornyek VALUES ('Groggo', 5, 'vedelem', 10875, 362, 725);
INSERT INTO Szornyek VALUES ('Verde', 1, 'tamadas', 9885, 812, 505);
INSERT INTO Szornyek VALUES ('Lushen', 6, 'tamadas', 9226, 900, 461);
INSERT INTO Szornyek VALUES ('Ritesh', 6, 'elet', 13500, 637, 604);
INSERT INTO Szornyek VALUES ('Molly', 8, 'tamogato', 11205, 494, 736);
INSERT INTO Szornyek VALUES ('Tarq', 2, 'tamadas', 8565, 714, 362);
INSERT INTO Szornyek VALUES ('Orion', 2, 'tamogato', 10215, 637, 659);
INSERT INTO Szornyek VALUES ('Veromos', 7, 'tamogato', 9225, 769, 758);
INSERT INTO Szornyek VALUES ('Vela', 5, 'vedelem', 10050, 681, 790);
INSERT INTO Szornyek VALUES ('Teshar', 3, 'tamadas', 7410, 1098, 549);
INSERT INTO Szornyek VALUES ('Jeanne', 3, 'elet', 12180, 615, 714);
INSERT INTO Szornyek VALUES ('Leo', 6, 'tamogato', 11850, 714, 637);

IF OBJECT_ID('JatekosSzornyKapcsolo', 'U') IS NOT NULL DROP TABLE JatekosSzornyKapcsolo;
CREATE TABLE JatekosSzornyKapcsolo (
    kapcsoloId INT IDENTITY(1,1) NOT NULL,
    jatekosId INT NOT NULL REFERENCES Jatekosok(jatekosId) ON DELETE CASCADE ON UPDATE CASCADE,
    szornyId INT NOT NULL REFERENCES Szornyek(szornyId) ON DELETE CASCADE ON UPDATE CASCADE
    CONSTRAINT KAPCSOLO_PRIMARY_KEY PRIMARY KEY (kapcsoloId));
    
INSERT INTO JatekosSzornyKapcsolo VALUES (1, 2);
INSERT INTO JatekosSzornyKapcsolo VALUES (1, 1);    
INSERT INTO JatekosSzornyKapcsolo VALUES (1, 15);
INSERT INTO JatekosSzornyKapcsolo VALUES (1, 8);
INSERT INTO JatekosSzornyKapcsolo VALUES (1, 4);
INSERT INTO JatekosSzornyKapcsolo VALUES (1, 7);
INSERT INTO JatekosSzornyKapcsolo VALUES (2, 7);
INSERT INTO JatekosSzornyKapcsolo VALUES (2, 14);
INSERT INTO JatekosSzornyKapcsolo VALUES (2, 12);
INSERT INTO JatekosSzornyKapcsolo VALUES (2, 5);
INSERT INTO JatekosSzornyKapcsolo VALUES (3, 1);
INSERT INTO JatekosSzornyKapcsolo VALUES (3, 8);
INSERT INTO JatekosSzornyKapcsolo VALUES (3, 14);
INSERT INTO JatekosSzornyKapcsolo VALUES (3, 15);
INSERT INTO JatekosSzornyKapcsolo VALUES (4, 4);
INSERT INTO JatekosSzornyKapcsolo VALUES (4, 1);
INSERT INTO JatekosSzornyKapcsolo VALUES (4, 3);
INSERT INTO JatekosSzornyKapcsolo VALUES (4, 9);
INSERT INTO JatekosSzornyKapcsolo VALUES (5, 10);
INSERT INTO JatekosSzornyKapcsolo VALUES (5, 11);
INSERT INTO JatekosSzornyKapcsolo VALUES (5, 7);
INSERT INTO JatekosSzornyKapcsolo VALUES (6, 5);
INSERT INTO JatekosSzornyKapcsolo VALUES (7, 3);
INSERT INTO JatekosSzornyKapcsolo VALUES (7, 1);
INSERT INTO JatekosSzornyKapcsolo VALUES (7, 3);
INSERT INTO JatekosSzornyKapcsolo VALUES (8, 13);
INSERT INTO JatekosSzornyKapcsolo VALUES (9, 12);
INSERT INTO JatekosSzornyKapcsolo VALUES (9, 9);
INSERT INTO JatekosSzornyKapcsolo VALUES (9, 1);
INSERT INTO JatekosSzornyKapcsolo VALUES (10, 7);
INSERT INTO JatekosSzornyKapcsolo VALUES (10, 4);
INSERT INTO JatekosSzornyKapcsolo VALUES (11, 12);
INSERT INTO JatekosSzornyKapcsolo VALUES (12, 3);
INSERT INTO JatekosSzornyKapcsolo VALUES (12, 15);
INSERT INTO JatekosSzornyKapcsolo VALUES (13, 8);
INSERT INTO JatekosSzornyKapcsolo VALUES (13, 12);
INSERT INTO JatekosSzornyKapcsolo VALUES (14, 13);
INSERT INTO JatekosSzornyKapcsolo VALUES (15, 14);
INSERT INTO JatekosSzornyKapcsolo VALUES (15, 6);
INSERT INTO JatekosSzornyKapcsolo VALUES (16, 6);