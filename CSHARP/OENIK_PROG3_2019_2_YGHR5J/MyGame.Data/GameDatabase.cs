﻿// <copyright file="GameDatabase.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyGame.Data
{
    /// <summary>
    /// Database.
    /// </summary>
    public class GameDatabase
    {
        private readonly GameDbEntities gameDb;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameDatabase"/> class.
        /// </summary>
        public GameDatabase()
        {
            this.gameDb = new GameDbEntities();
        }

        /// <summary>
        /// Gets game data.
        /// </summary>
        public static GameDbEntities GameDb { get; }
    }
}
