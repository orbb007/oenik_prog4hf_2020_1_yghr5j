﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MyGame.Wpf
{
    class MainVm : ViewModelBase
    {
        MainLogic logic;
        private MonsterVm _selectedMonster;
        private ObservableCollection<MonsterVm> _allMonsters;

        public MonsterVm SelectedMonster { get => _selectedMonster; set => Set(ref _selectedMonster, value); }

        public ObservableCollection<MonsterVm> AllMonsters { get => _allMonsters; set => Set(ref _allMonsters, value); }

        public ICommand AddCommand { get; private set; }
        public ICommand DeleteCommand { get; private set; }
        public ICommand ModifyCommand { get; private set; }
        public ICommand LoadCommand { get; private set; }
        
        public Func<MonsterVm, bool> EditorFunction { get; set; }

        public MainVm()
        {
            logic = new MainLogic();
            AllMonsters = new ObservableCollection<MonsterVm>();
            AddCommand = new RelayCommand(() => logic.EditMonster(null, EditorFunction));
            ModifyCommand = new RelayCommand(() => logic.EditMonster(_selectedMonster, EditorFunction));
            DeleteCommand = new RelayCommand(() => logic.ApiDeleteMonster(_selectedMonster));
            LoadCommand = new RelayCommand(() => AllMonsters = new ObservableCollection<MonsterVm>(logic.ApiGetMonsters()));
        }


    }
}
