﻿using GalaSoft.MvvmLight.Messaging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MyGame.Wpf
{
    class MainLogic
    {
        string url = "http://localhost:54904/api/MonstersApi/";
        HttpClient client = new HttpClient();
        public List<MonsterVm> ApiGetMonsters()
        {
            string json = client.GetStringAsync(url + "all").Result;
            var list = JsonConvert.DeserializeObject<List<MonsterVm>>(json);
            return list;
        }
        public void ApiDeleteMonster(MonsterVm monster)
        {
            bool success = false;
            if (monster != null)
            {
                string json = client.GetStringAsync(url + "delete/" + monster.Id).Result;
                JObject obj = JObject.Parse(json);
                success = (bool)obj["operationResult"];
            }
            SendMessage(success);
        }

        bool ApiEditMonster(MonsterVm monster, bool isEditing)
        {
            if (monster == null)
            {
                return false;
            }
            string url2 = isEditing ? url + "modify" : url + "add";
            Dictionary<string, string> postData = new Dictionary<string, string>();
            if (isEditing)
            {
                postData.Add(nameof(MonsterVm.Id), monster.Id.ToString());
            }
            postData.Add(nameof(MonsterVm.Name), monster.Name);
            postData.Add(nameof(MonsterVm.ElementId), monster.ElementId.ToString());
            postData.Add(nameof(MonsterVm.Type), monster.Type);
            postData.Add(nameof(MonsterVm.HitPoints), monster.HitPoints.ToString());
            postData.Add(nameof(MonsterVm.Attack), monster.Attack.ToString());
            postData.Add(nameof(MonsterVm.Defense), monster.Defense.ToString());

            string json = client.PostAsync(url2, new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
            JObject obj = JObject.Parse(json);
            return (bool)obj["operationResult"];
        }
        public void EditMonster(MonsterVm monster, Func<MonsterVm, bool> editor)
        {
            MonsterVm clone = new MonsterVm();
            if (monster != null)
            {
                clone.CopyFrom(monster);
            }
            bool? success = editor?.Invoke(clone);
            if (success == true)
            {
                if (monster != null)
                {
                    success = ApiEditMonster(clone, true);
                }
                else
                {
                    success = ApiEditMonster(clone, false);
                }
            }
            SendMessage(success == true);
        }


        void SendMessage(bool success)
        {
            string message = success ? "Művelet sikeresen végrehajtva" : "Művelet sikertelen";

            Messenger.Default.Send(message, "MonsterResult");
        }
    }
}
