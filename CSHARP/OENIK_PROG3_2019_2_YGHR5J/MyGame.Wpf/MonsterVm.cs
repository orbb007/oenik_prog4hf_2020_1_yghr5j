﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyGame.Wpf
{
    class MonsterVm : ObservableObject
    {
        private int _id;
        private string _name;
        private int _elementId;
        private string _type;
        private int _attack;
        private int _defense;
        private int _hitPoints;

        public int Id { get => _id; set => Set(ref _id, value); }
        public string Name { get => _name; set => Set(ref _name, value); }
        public int ElementId { get => _elementId; set => Set(ref _elementId, value); }
        public string Type { get => _type; set => Set(ref _type, value); }
        public int Attack { get => _attack; set => Set(ref _attack, value); }
        public int Defense { get => _defense; set => Set(ref _defense, value); }
        public int HitPoints { get => _hitPoints; set => Set(ref _hitPoints, value); }

        public void CopyFrom(MonsterVm other)
        {
            if (other == null)
            {
                return;
            }
            this.Id = other.Id;
            this.Name = other.Name;
            this.Type = other.Type;
            this.ElementId = other.ElementId;
            this.Attack = other.Attack;
            this.Defense = other.Defense;
            this.HitPoints = other.HitPoints;
        }
    }
}
