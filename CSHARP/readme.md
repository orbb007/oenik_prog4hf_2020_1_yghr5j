#CSHARP projekt

Projekt c�m: MyGame

T�bl�k:
J�t�kosok (id, n�v, kezd�s_d�tuma, szint, kl�n, rang)
Sz�rnyek (id, n�v, elem_id, t�pus, �let, t�mad�s, v�delem)
Elemek (id, n�v, sz�n, er�ss�g, ellent�t, ritkas�g)
J�t�kosSz�rnyKapcsol� (j�t�kos_id, sz�rny_id)


Funkci�k:
- J�t�kosok list�z�sa / hozz�ad�sa / m�dos�t�sa / t�rl�se
- Sz�rnyek list�z�sa / hozz�ad�sa / m�dos�t�sa / t�rl�se
- Elemek list�z�sa / hozz�ad�sa / m�dos�t�sa / t�rl�se
- J�t�kosSz�rnyKapcsol� list�z�sa / hozz�ad�sa / m�dos�t�sa \
  t�rl�se
- Egy j�t�kos osszes sz�rny�nek list�z�sa
- Ki�rni a sz�rnyek t�pusak�nt csoportos�tva azt, hogy
  melyik t�pusb�l mennyi van a j�t�kosoknak
- Ki�rni elemenk�nt a sz�rnyek �tlagos �let�t, t�mad�s�t �s 
  v�delm�t